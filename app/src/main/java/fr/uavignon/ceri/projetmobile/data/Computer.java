package fr.uavignon.ceri.projetmobile.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.util.List;
import java.util.Map;

@Entity(tableName = "ceri_museum", indices = {@Index(value = {"name"}, unique = true)})
public class Computer implements Comparable<Computer> {

    public static final String TAG = Computer.class.getSimpleName();

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    private String id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "description")
    private String description;

    @NonNull
    @ColumnInfo(name = "timeFrame")
    private String timeFrame;

    @NonNull
    @ColumnInfo(name = "categories")
    private String categories;

    @ColumnInfo(name = "technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name = "year")
    private int year;

    @ColumnInfo(name = "brand")
    private String brand;

    @ColumnInfo(name = "pictures")
    private String pictures;

    @ColumnInfo(name = "working")
    private String working;

    public Computer(@NonNull String id, @NonNull String name,
                    @NonNull String description, @NonNull String timeFrame,
                    @NonNull String categories) {
        this.name = name;
        this.id = id;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(@NonNull String timeFrame) {
        this.timeFrame = timeFrame;
    }

    @NonNull
    public String getCategories() {
        return categories;
    }

    public void setCategories(@NonNull String categories) {
        this.categories = categories;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures.toString();
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public void setWorking(boolean working) {
        if (working) this.working = "oui";
        else this.working = "non";
    }

    @Override
    public int compareTo(Computer o) {
        return -1*(this.getYear() - o.getYear());
    }

    public int compareToAlpha(Computer o) {
        return this.getName().compareTo(o.getName());
    }
}
