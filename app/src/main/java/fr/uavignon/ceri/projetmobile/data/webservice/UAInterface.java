package fr.uavignon.ceri.projetmobile.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UAInterface {
    @GET("/cerimuseum/{path}")
    Call<Map<String, ItemResponse>> getCollection(@Path("path") String path);

    @GET("/cerimuseum/{path}")
    Call<Double> getVersion(@Path("collectionversion") String path);

    @GET("/cerimuseum/items/{path}")
    Call<ItemDetailedResponse> getItem(@Path("path") String path);
}
