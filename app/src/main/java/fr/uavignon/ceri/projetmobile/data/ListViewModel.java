package fr.uavignon.ceri.projetmobile.data;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class ListViewModel extends AndroidViewModel {
    private CeriMuseumRepository repository;
    private LiveData<List<Computer>> allComputers;
    //private List<Computer> allComputersSynch;

    public ListViewModel (Application application) {
        super(application);
        repository = CeriMuseumRepository.get(application);
        allComputers = repository.getAllComputer();
        //allComputersSynch = repository.getAllComputerSynch();
    }

    public LiveData<List<Computer>> getAllComputers() {
        return allComputers;
    }

    //public List<Computer> getAllComputersSynch() {
    //    return allComputersSynch;
    //}
}
