package fr.uavignon.ceri.projetmobile.data.webservice;

import java.util.List;
import java.util.Map;

public class ItemDetailedResponse {

    public final String name = null;
    public final List<String> categories = null;
    public final List<Integer> timeFrame = null;
    public final String description = null;

    public final List<String> technicalDetails = null;
    public final String brand = null;
    public final Boolean working = false;
    public final Integer year = 0;
    public final Map<String, String> pictures = null;



}
