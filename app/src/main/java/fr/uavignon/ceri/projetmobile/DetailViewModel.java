package fr.uavignon.ceri.projetmobile;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.projetmobile.data.Computer;
import fr.uavignon.ceri.projetmobile.data.CeriMuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private CeriMuseumRepository repository;
    private MutableLiveData<Computer> computer;


    public DetailViewModel (Application application) {
        super(application);
        repository = CeriMuseumRepository.get(application);
        computer = new MutableLiveData<>();

    }

    public void setCity(String id) {
        repository.getComputer(id);
        computer = repository.getSelectedComputer();
    }

    LiveData<Computer> getComputer() {
        return computer;
    }
}

