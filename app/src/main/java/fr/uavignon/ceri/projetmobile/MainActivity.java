package fr.uavignon.ceri.projetmobile;

import android.os.Build;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import fr.uavignon.ceri.projetmobile.data.CeriMuseumRepository;
import fr.uavignon.ceri.projetmobile.data.Computer;
import fr.uavignon.ceri.projetmobile.data.ListViewModel;
import fr.uavignon.ceri.projetmobile.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        ListViewModel viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        List<Computer> list = viewModel.getAllComputers().getValue();
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_tout) {
            //adapter.setComputerList(list);
            return true;
        }
        else if (id == R.id.action_8bits) {

            List<Computer> result = new ArrayList<Computer>();
            list.stream().filter(o -> o.getCategories().contains("bits")).forEach(
                    o -> {
                        result.add(o);
                    }
            );
            //adapter.setComputerList(list);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}