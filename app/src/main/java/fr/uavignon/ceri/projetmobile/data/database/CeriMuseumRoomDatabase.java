package fr.uavignon.ceri.projetmobile.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.projetmobile.data.Computer;

@Database(entities = {Computer.class}, version = 1, exportSchema = false)
public abstract class CeriMuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = CeriMuseumRoomDatabase.class.getSimpleName();

    public abstract ComputerDao computerDao();

    private static CeriMuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;

    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static CeriMuseumRoomDatabase getDatabase(final Context context) {
        //context.deleteDatabase("ceri_database");
        if (INSTANCE == null) {
            synchronized (CeriMuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CeriMuseumRoomDatabase.class, "ceri_database")
                                    //.addCallback(sRoomDatabaseCallback)
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ComputerDao dao = INSTANCE.computerDao();
                        dao.deleteAll();

                        Computer[] ceriMuseum = {new Computer("yzw", "Câbles et terminateurs SCSI", "test","2000",""),
                                new Computer("fff", "nom ordi 2", "description ordi 2","2000",""),
                                new Computer("ddd", "nom ordi 3", "description ordi 3","2000",""),
                                new Computer("sss", "nom ordi 4", "description ordi 4","2000",""),
                                new Computer("aaa", "nom ordi 5", "description ordi 5","2000","")};

                        for(Computer newComputer : ceriMuseum)
                            dao.insert(newComputer);
                        Log.d(TAG,"database populated");
                    });

                }
            };
}

