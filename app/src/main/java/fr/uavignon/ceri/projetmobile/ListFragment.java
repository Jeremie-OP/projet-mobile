package fr.uavignon.ceri.projetmobile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.uavignon.ceri.projetmobile.data.CeriMuseumRepository;
import fr.uavignon.ceri.projetmobile.data.Computer;
import fr.uavignon.ceri.projetmobile.data.ListViewModel;


public class ListFragment extends Fragment {

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progressBar;
    private int order = 0;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        layoutManager = new LinearLayoutManager(getActivity());


        CeriMuseumRepository ceriMuseumRepository = new CeriMuseumRepository(getActivity().getApplication());
        ceriMuseumRepository.loadCollection();

        recyclerSetup();
        observerSetup();

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (order == 0) {
                    List<Computer> test = viewModel.getAllComputers().getValue();
                    Collections.sort(test,Computer::compareTo);
                    adapter.setComputerList(test);
                    order = 1;
                    fab.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_menu_sort_alphabetically));

                }
                else {
                    List<Computer> test = viewModel.getAllComputers().getValue();
                    Collections.sort(test,Computer::compareToAlpha);
                    adapter.setComputerList(test);
                    order = 0;
                    fab.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_menu_sort_by_size));
                }
            }
        });

    }

    private void recyclerSetup() {
        adapter = new RecyclerAdapter(viewModel.getAllComputers().getValue());
        recyclerView = getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void observerSetup() {
        viewModel.getAllComputers().observe(getViewLifecycleOwner(),
                new Observer<List<Computer>>() {
                    @Override
                    public void onChanged(@Nullable final List<Computer> computers) {
                        adapter.setComputerList(computers);
                    }
                });
    }

}