package fr.uavignon.ceri.projetmobile.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.ListIterator;

import fr.uavignon.ceri.projetmobile.data.Computer;

@Dao
public interface ComputerDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Computer computer);

    @Update
    int update(Computer computer);


    @Query("DELETE FROM ceri_museum")
    void deleteAll();

    @Query("SELECT * from ceri_museum ORDER BY name ASC")
    LiveData<List<Computer>> getAllComputer();

    //@Query("SELECT * from ceri_museum ORDER BY name ASC")
    //List<Computer> getAllComputerSynch();

    @Query("SELECT * FROM ceri_museum WHERE _id= :id")
    Computer getComputerById(String id);
}
