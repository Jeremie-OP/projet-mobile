package fr.uavignon.ceri.projetmobile;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.projetmobile.data.Computer;
import fr.uavignon.ceri.projetmobile.data.ListViewModel;


public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.projetmobile.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.projetmobile.RecyclerAdapter.class.getSimpleName();

    private List<Computer> computerList;
    private ListViewModel listViewModel;

    public static final int ITEM_LEFT_ALIGN = 0;
    public static final int ITEM_RIGHT_ALIGN = 1;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
            return new ViewHolder(v);
        }
        else {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_v2, viewGroup, false);
            return new ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        int viewType = getItemViewType(i);
        if (computerList.get(i).getName() != "") viewHolder.itemName.setText(computerList.get(i).getName());
        else viewHolder.itemName.setText(computerList.get(i).getBrand());
        viewHolder.itemDescription.setText(computerList.get(i).getDescription());
        viewHolder.itemCategorie.setText(computerList.get(i).getCategories());
        viewHolder.itemCategorie.setTextColor(-16776961 );
        Glide.with(viewHolder.itemView).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+computerList.get(i).getId() + "/thumbnail").into(viewHolder.itemImage);
    }

    @Override
    public int getItemCount() {
        return computerList == null ? 0 : computerList.size();
    }

    public int getItemViewType(int position) {
        if (position % 2 == 0) return ITEM_LEFT_ALIGN;
        else return ITEM_RIGHT_ALIGN;
    }

    public void setComputerList(List<Computer> computers) {
        Computer test = new Computer("999","","","","");
        computerList = computers;
        computerList.add(test);
        notifyDataSetChanged();
        computers.remove(test);
        notifyDataSetChanged();
    }

    public void setListViewModel(ListViewModel listViewModel) {
        this.listViewModel = listViewModel;
    }

    public RecyclerAdapter(List<Computer> computerList) {
        this.computerList = computerList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView itemDescription;
        ImageView itemImage;
        TextView itemCategorie;

        ActionMode actionMode;
        long idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemDescription = itemView.findViewById(R.id.item_description);
            itemImage = itemView.findViewById(R.id.item_image);
            itemCategorie = itemView.findViewById(R.id.item_categorie);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.computerList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setComputerId(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });

        }

    }
}
