package fr.uavignon.ceri.projetmobile.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.projetmobile.MainActivity;
import fr.uavignon.ceri.projetmobile.data.database.CeriMuseumRoomDatabase;
import fr.uavignon.ceri.projetmobile.data.database.ComputerDao;
import fr.uavignon.ceri.projetmobile.data.webservice.ItemDetailedResponse;
import fr.uavignon.ceri.projetmobile.data.webservice.ItemResponse;
import fr.uavignon.ceri.projetmobile.data.webservice.UAInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.projetmobile.data.database.CeriMuseumRoomDatabase.databaseWriteExecutor;

public class CeriMuseumRepository {

    private static final String TAG = CeriMuseumRepository.class.getSimpleName();

    private LiveData<List<Computer>> allComputer;
    //private List<Computer> allComputerSynch;

    private MutableLiveData<Computer> selectedComputer;
    private double version;

    private ComputerDao computerDao;
    private Application application;

    private final UAInterface api;

    private static volatile CeriMuseumRepository INSTANCE;

    public synchronized static CeriMuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CeriMuseumRepository(application);
        }
        return INSTANCE;
    }

    public CeriMuseumRepository(Application application) {
        CeriMuseumRoomDatabase db = CeriMuseumRoomDatabase.getDatabase(application);
        this.application = application;
        computerDao = db.computerDao();
        allComputer = computerDao.getAllComputer();
        //allComputerSynch = computerDao.getAllComputerSynch();
        selectedComputer = new MutableLiveData<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        api = retrofit.create(UAInterface.class);
    }

    public LiveData<List<Computer>> getAllComputer() {
        return allComputer;
    }

    //public List<Computer> getAllComputerSynch() {
    //    return allComputerSynch;
    //}

    public MutableLiveData<Computer> getSelectedComputer() {
        return selectedComputer;
    }

    public long insertComputer(Computer newComputer) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return computerDao.insert(newComputer);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedComputer.setValue(newComputer);
        return res;
    }

    public long updateComputer(Computer computer) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return computerDao.update(computer);
        });
        long res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedComputer.setValue(computer);
        return res;
    }

    public void getComputer(String id) {
        Future<Computer> fcomputer = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return computerDao.getComputerById(id);
        });
        try {
            selectedComputer.setValue(fcomputer.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private double getOldVersion() {
        return 641917235.773294;
    }

    public void loadCollectionVersion() {
        api.getVersion("collectionversion").enqueue(
                new Callback<Double>() {
                    @Override
                    public void onResponse(Call<Double> call, Response<Double> response) {
                        if (response.body() > getOldVersion()) {
                            application.getSharedPreferences("VERSION", Context.MODE_PRIVATE).edit().putString("double", response.body().toString()).apply();
                            version = getOldVersion();
                            loadCollection();
                        }
                    }

                    @Override
                    public void onFailure(Call<Double> call, Throwable t) {

                    }
                }
        );
    }

    public void loadCollection() {
        api.getCollection("collection").enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {
                        Log.d(TAG, String.format("Collection response obtained"));
                        for (Map.Entry<String, ItemResponse> entry : response.body().entrySet()) {
                            Computer newComputer = new Computer(entry.getKey(),
                                                                entry.getValue().name,
                                                                entry.getValue().description,
                                                                entry.getValue().timeFrame.toString(),
                                                                entry.getValue().categories.toString());

                            insertComputer(newComputer);


                            api.getItem(entry.getKey()).enqueue(
                                    new Callback<ItemDetailedResponse>() {
                                        @Override
                                        public void onResponse(Call<ItemDetailedResponse> call, Response<ItemDetailedResponse> response) {
                                            if (response.body().working) newComputer.setWorking("oui");
                                            else newComputer.setWorking("non");
                                            newComputer.setWorking(response.body().working);
                                            newComputer.setYear(response.body().year);
                                            if (response.body().technicalDetails != null) {
                                                newComputer.setTechnicalDetails(response.body().technicalDetails.toString());
                                            }
                                            if (response.body().pictures != null) {
                                                newComputer.setPictures(response.body().pictures.toString());
                                            }
                                            newComputer.setBrand(response.body().brand);
                                            updateComputer(newComputer);
                                        }

                                        @Override
                                        public void onFailure(Call<ItemDetailedResponse> call, Throwable t) {

                                        }
                                    }
                            );


                        }
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d(TAG, String.format("Error on collection response"));
                    }
                }
        );
    }


}
