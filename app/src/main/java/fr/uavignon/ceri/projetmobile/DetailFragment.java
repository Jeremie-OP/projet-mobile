package fr.uavignon.ceri.projetmobile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.Map;


public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textComputerName, textComputerDescription, textComputerCategorie, textComputerYear, textComputerWorking, textComputerAdvanced, textComputerImage1;
    private ImageView imageComputerThumbnail, imageComputer1;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String computerID = args.getComputerId();
        viewModel.setCity(computerID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textComputerName = getView().findViewById(R.id.computer_name);
        textComputerDescription = getView().findViewById(R.id.computer_description);
        imageComputerThumbnail = getView().findViewById(R.id.computer_thumbnail);
        textComputerCategorie = getView().findViewById(R.id.computer_categorie);
        textComputerYear = getView().findViewById(R.id.computer_year);
        textComputerWorking = getView().findViewById(R.id.computer_working);
        textComputerAdvanced = getView().findViewById(R.id.computer_advanced);
        textComputerImage1 = getView().findViewById(R.id.computer_image1);
        imageComputer1 = getView().findViewById(R.id.computer_img1);

    }

    private void observerSetup() {
        viewModel.getComputer().observe(getViewLifecycleOwner(),
                computer -> {
                    if (computer != null) {
                        Log.d(TAG, "observing city view");
                        textComputerName.setText(computer.getName());
                        textComputerDescription.setText(computer.getDescription());
                        Glide.with(this).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+computer.getId() + "/thumbnail").into(imageComputerThumbnail);
                        if (computer.getWorking() == "oui") textComputerWorking.setText("FONCTIONNEL");
                        else textComputerWorking.setText("NON FONCTIONNEL");
                        if (computer.getYear() != 0) textComputerYear.setText(computer.getYear()+"");
                        else textComputerYear.setText("INCONNUE");
                        textComputerCategorie.setText(computer.getCategories());
                        if(computer.getTechnicalDetails() != null ){
                            getView().findViewById(R.id.computer_adv).setVisibility(View.VISIBLE);
                            textComputerAdvanced.setVisibility(View.VISIBLE);
                            textComputerAdvanced.setText(computer.getTechnicalDetails());
                        }
                        else {
                            getView().findViewById(R.id.computer_adv).setVisibility(View.INVISIBLE);
                            textComputerAdvanced.setVisibility(View.INVISIBLE);
                        }
                        if (computer.getPictures() != null){

                            String tmp = computer.getPictures();
                            Map<String, String> result = stringToMap(tmp);
                            Glide.with(this)
                                    .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+computer.getId() + "/images/" + (String)result.keySet().toArray()[0])
                                    .into(imageComputer1);
                            textComputerImage1.setText((String)result.values().toArray()[0]);
                            textComputerImage1.setVisibility(View.VISIBLE);
                            imageComputer1.setVisibility(View.VISIBLE);
                        }

                    }
                });
    }

    private Map<String,String> stringToMap(String tmp) {
        Map<String,String> result = new HashMap<>();
        String key,value = "";
        while (tmp.length() > 2) {
            key = tmp.substring(1, tmp.indexOf('='));
            if (tmp.contains(".,")) {
                if (tmp.contains("=,") && tmp.indexOf('=') == tmp.indexOf("=,")) {
                    value = "";
                    tmp = tmp.substring(tmp.indexOf("=,") + 2);
                }
                else {
                    value = tmp.substring(tmp.indexOf('=') + 1, tmp.indexOf(".,") + 1);
                    tmp = tmp.substring(tmp.indexOf(".,") + 2);
                }
            }
            else {
                value = tmp.substring(tmp.indexOf('=') + 1, tmp.indexOf(".}") + 1);
                result.put(key,value);
                break;
            }
            result.put(key,value);
        }
        return result;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}